from scipy.io.wavfile import read, write

import numpy as np

[Fr, Dat] = read('SA31NS.wav')

noise_spans = []
noise_spans.extend(Dat[500:1000])
noise_spans.extend(Dat[2500:2700])
noise_spans.extend(Dat[6200:6750])
noise_spans.extend(Dat[7000:7350])
noise_spans.extend(Dat[9600:9800])
noise_spans.extend(Dat[9800:10100])
noise_spans.extend(Dat[10200:10350])
noise_spans.extend(Dat[13000:16000])
noise_spans.extend(Dat[18500:19000])
noise_spans.extend(Dat[19000:19500])
noise_spans.extend(Dat[19600:20000])
noise_spans.extend(Dat[24500:25200])
noise_spans.extend(Dat[26050:26450])
noise_spans.extend(Dat[26800:27100])
noise_spans.extend(Dat[27900:28100])
noise_spans.extend(Dat[31500:34000])
noise_spans.extend(Dat[36450:36700])
noise_spans.extend(Dat[37600:38000])
noise_spans.extend(Dat[38500:39100])
noise_spans.extend(Dat[42000:46000])
noise_spans.extend(Dat[48000:50000])

noise = np.array(noise_spans)

write('noise.wav', Fr, np.int16(noise))