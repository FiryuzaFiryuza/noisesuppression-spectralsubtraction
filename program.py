from scipy import signal as SGN
from scipy.io.wavfile import read, write

import matplotlib.pyplot as plt
import numpy as np

[Fr, Signal] = read('SA31NS.wav')
[Fr_n, Noise] = read('noise.wav')

span_len = np.int(2 * round(Fr / 100)) # use 20 ms (na 50)
C = 6

def variance_noise_fft(noise, span_len):
    part_len = span_len / 2
    noise_len = len(noise)
    variance_values = []
    fft_coefs = []
    count_span = 0
    for idx in xrange(0, noise_len, span_len):
        span = np.float32(noise[idx:idx + span_len])
        fft = abs(np.fft.fft(span))[0:part_len]
        fft_coefs.append(fft)
        count_span += 1

    fft_coefs_matrix = np.zeros((count_span, part_len))
    i = 0
    # [fft_coefs_matrix[i][j] = arr[1][j] for j in xrange(len(arr[1]))] i+= 1 for arr in enumerate(fft_coefs)]
    for arr in enumerate(fft_coefs):
        for j in xrange(len(arr[1])):
            fft_coefs_matrix[i][j] = arr[1][j]
        i += 1

    fft_coefs_t = np.transpose(fft_coefs_matrix)
    for row in xrange(fft_coefs_t.shape[0]):
        mean = np.sum(fft_coefs_t[row]) / count_span
        variance = np.sum(np.power(fft_coefs_t[row] - mean, 2)) / count_span
        variance_values.append(variance)


    variance_values = np.array(variance_values)
    out = variance_values / count_span

    return out


def fft_signal(signal, span_len):
    part_len = span_len / 2
    signal_len = len(signal)
    fft_coefs_matrix = []
    abs_fft_coefs_matrix = []
    sqr_fft_coefs_matrix = []

    for idx in xrange(0, signal_len - span_len, part_len):
        span = np.float32(signal[idx:idx + span_len])
        fft_coefs = np.fft.fft(span)
        fft_coefs_matrix.append(fft_coefs[0:part_len])

        abs_fft_coefs = abs(fft_coefs[0:part_len])
        abs_fft_coefs_matrix.append(abs_fft_coefs)

        fft = abs_fft_coefs
        sqr = fft * fft
        sqr_fft_coefs_matrix.append(sqr)

    fft_coefs_matrix = np.array(fft_coefs_matrix)
    abs_fft_coefs_matrix = np.array(abs_fft_coefs_matrix)
    sqr_fft_coefs_matrix = np.array(sqr_fft_coefs_matrix)

    return fft_coefs_matrix, sqr_fft_coefs_matrix, abs_fft_coefs_matrix


def Hamming_window_smooth(signal):
    smooth_signal = []
    count_span = signal.shape[0]
    if count_span > 1:
        N = signal.shape[1]
        half_N = N // 2
        for i in xrange(count_span - 1):
            window = SGN.hamming(N)
            part_span_1 = window[half_N:]*signal[i][half_N:]
            part_span_2 = window[:half_N]*signal[i+1][:half_N]

            span = np.array(part_span_1 + part_span_2)

            signal[i] = np.concatenate([signal[i][0:half_N], span])
            signal[i+1] = np.concatenate([span, signal[i+1][half_N:]])
            smooth_signal.extend(signal[i])
            if i == (count_span - 2):
                smooth_signal.extend(signal[i + 1])

    return np.array(smooth_signal)


def remove_noise(signal, noise, span_len, C):
    part_span = span_len / 2
    variance_noise = variance_noise_fft(noise, span_len)
    fft_coefs, sqr_signal, abs_fft_coefs = fft_signal(signal, span_len)
    result = []

    for row in xrange(sqr_signal.shape[0]):
        diff = sqr_signal[row] - C * variance_noise
        no_noise = np.zeros(span_len)
        for idx in xrange(part_span):
            if diff[idx] > 0:
                no_noise[idx] = np.sqrt(diff[idx])

        no_noise[part_span:] = no_noise[:part_span][::-1]
        no_noise[0] = 0
        i_signal = np.fft.ifft(no_noise)
        real_i_signal = np.real(i_signal)
        result.append(real_i_signal)

    result = np.array(result)
    out = Hamming_window_smooth(result)

    return out


result = remove_noise(Signal, Noise, span_len, C)
write('no_noise_new.wav', Fr, np.int16(result))
